from __future__ import print_function # In python 2.7
import sys

from flask import Flask, redirect, url_for, request, render_template
from flask_googlemaps import GoogleMaps
from flask_googlemaps import Map, icons
from maprunner.maps import map_exists, load_maps, get_centre

app = Flask(__name__)
app.config['GOOGLEMAPS_KEY'] = "AIzaSyA3aAplFrhyb_KEr4LAy74fGsof0NdSsPc"

GoogleMaps(app, key="AIzaSyAZzeHhs-8JZ7i18MjFuM35dJHq70n3Hx4")


@app.route('/')
def init_redirect():
    return redirect(url_for('index'))


@app.route('/index')
def index():
    return render_template('login.html')


@app.route('/map/<filename>')
def mapView(filename):
    if not map_exists(filename):
        return render_template('500.html', error="File " + filename + " does not exist")

    list = load_maps(filename)


    polyline = {
        'stroke_color': '#0AB0DE',
        'stroke_opacity': 1.0,
        'stroke_weight': 4,
        'path': list
    }

    p = get_centre(list)

    plinemap = Map(
        identifier="plinemap",
        varname="plinemap",
        lat=p.x,
        lng=p.y,
        style="width:100%;height:100%",
        polylines=[polyline]
    )

    return render_template(
        'example.html', plinemap=plinemap)


@app.route('/success/<name>')
def success(name):
    return 'welcome %s' % name


@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        user = request.form['nm']
        return redirect(url_for('success', name=user))
    else:
        user = request.args.get('nm')
        return redirect(url_for('success', name=user))


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


if __name__ == '__main__':
    app.run(debug=True)