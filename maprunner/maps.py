from pathlib2 import Path
import gpxpy
import collections



def map_exists(name):
    my_file = Path("maps/" + name)
    if my_file.is_file():
        return True
    return False


def load_maps(name):
    f = open('maps/' + name, 'r')
    gpx = gpxpy.parse(f)

    list = []
    for track in gpx.tracks:
        for segment in track.segments:
            for point in segment.points:
                list.append({'lat': point.latitude, 'lng': point.longitude})

    return list


def get_centre(list):
    Point = collections.namedtuple('Point', ['x', 'y'])
    xmin = min(list, key=lambda x: x['lat'])['lat']
    xmax = max(list, key=lambda x: x['lat'])['lat']

    ymin = min(list, key=lambda x: x['lng'])['lng']
    ymax = max(list, key=lambda x: x['lng'])['lng']

    p = Point((xmin + xmax)/2, y=(ymax + ymin)/2)
    return p
